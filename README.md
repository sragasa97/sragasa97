Hey there! 👋 I'm Shaun, a Filipino-American, born, raised, and based in South Florida. 🌴☀️
> I am a full-stack software engineer with a drive for life-long learning and improving my skillset. I have experience developing several full-stack applications in an Agile environment.

About a year ago, I realized that what I was currently studying at the time, nursing, did not inspire any passion in me, And so, I decided to take a chance on myself, and transition into a field that, while I had no experience, sparked a passion for learning and creativity within me once more.
During my coding journey, I completed a rigorous 19-week software engineering immersive program where I found that I flourish in collaborative environments where my team and I can find creative and effective solutions to problems at hand.

As a POC, I value diversity, equity, and inclusivity within the workplace. I believe in creating belonging by setting intention - hearing the voices and unique perspectives of others to create a space where marginalized or minority groups can feel safe. I believe this approach ultimately shapes a workplace into one that produces effective and unique solutions to issues of any kind.

🖥 Technical Skillset 🖥
- Programming Languages || Python 3 • JavaScript ES6+ • TypeScript • SQL • HTML5 • CSS
- Front-End || React • React Hooks • Bootstrap • Tailwind CSS • Redux.js • Node.js • DOM Manipulation
- Back-End || Django 4 • FastAPI • MySQL • PostgreSQL • MongoDB • RabbitMQ
- System Design || RESTful API Design • Monoliths • Microservices • Domain-driven Design
- Deployment/Dev Tools || Docker • Git • Github • Gitlab • Unit Testing • Agile Workflow • Insomnia • VSCode • Jira